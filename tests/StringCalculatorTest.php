<?php

use PHPUnit\Framework\TestCase;
use Jaworek\StringCalculator\StringCalculator;

class StringCalculatorTest extends TestCase
{
    private $stringCalculator;

    public function setUp()
    {
        $this->stringCalculator = StringCalculator::instantiate();
    }

    public function testAddEmptyString()
    {
        $sum = $this->stringCalculator->add("");
        $this->assertEquals(0, $sum);
    }

    public function testAddString()
    {
        $sum = $this->stringCalculator->add("test");
        $this->assertEquals(0, $sum);
    }

    public function testAddOneNumber()
    {
        $sum = $this->stringCalculator->add("1");
        $this->assertEquals(1, $sum);
    }

    public function testAddTwoNumbers()
    {
        $sum = $this->stringCalculator->add("1,2");
        $this->assertEquals(3, $sum);
    }

    public function testAddMultipleNumbers()
    {
        $sum = $this->stringCalculator->add("2,2,2");
        $this->assertEquals(6, $sum);
    }

    public function testNewLineDelimiter()
    {
        $sum = $this->stringCalculator->add("1\n2,3");
        $this->assertEquals(6, $sum);
    }

    public function testAddBiggerThan1000()
    {
        $sum = $this->stringCalculator->add("1,2,1001");
        $this->assertEquals(3, $sum);
    }

    public function testAddNegative()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("Numbers can't be lower than 0. Not allowed numbers: -10.");

        $this->stringCalculator->add("1,-10");
    }

    public function testAddMultipleNegative()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("Numbers can't be lower than 0. Not allowed numbers: -1,-2.");
        $this->stringCalculator->add("1,-1,2,-2");
    }

    public function testCustomSeparator()
    {
        $sum = $this->stringCalculator->add("//[;]\n1;2;3;4");
        $this->assertEquals(10, $sum);
    }

    public function testMultipleCustomSeparators()
    {
        $sum = $this->stringCalculator->add("//[..][|]\n1|2..3|4");
        $this->assertEquals(10, $sum);
    }

    public function testEmptySeparator()
    {
        $sum = $this->stringCalculator->add("//");
        $this->assertEquals(0, $sum);
    }
}