<?php

namespace Jaworek\StringCalculator\Exception;

use InvalidArgumentException as PhpInvalidArgumentException;

class InvalidArgumentException extends PhpInvalidArgumentException
{
    
}