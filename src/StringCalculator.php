<?php

namespace Jaworek\StringCalculator;

use Jaworek\StringCalculator\Exception\InvalidArgumentException;
use Jaworek\StringCalculator\Validator\AllowOnlyPositive;
use Jaworek\StringCalculator\Modifier\ModifierInterface;
use Jaworek\StringCalculator\Modifier\SkipLargerThan;
use Jaworek\StringCalculator\Parser\ParserInterface;
use Jaworek\StringCalculator\Parser\ReplaceExplode;
use Jaworek\StringCalculator\Validator\ValidatorInterface;

/**
 * Main class of package. Use add method to calculate sum. 
 */
class StringCalculator
{
    /**
     * Array of numbers to sum up
     * @var int[]
     */
    private $numbers = [];

    /**
     * Input string parser
     * @var ParserInterface
     */
    private $parser;

    /**
     * Array with number modifiers
     * @var ModifierInterface[]
     */
    private $modifiers = [];

    /**
     * Array with validators
     * @var ValidatorInterface
     */
    private $validators = [];

    public function __construct(ParserInterface $parser)
    {
        $this->parser = $parser;
    }

    /**
     * Calculates sum of numbers given as a parameter
     *
     * Numbers must be in specific format. Look at readme.md for more information.
     * 
     * @param string $stringWithNumbers
     * @throws InvalidArgumentException
     * @return int sum
     */
    public function add($stringWithNumbers)
    {
        $this->parseString($stringWithNumbers);

        $this->applyModifiers();

        $this->runValidators();

        return $this->calculateSum();
    }

    /**
     * Transforms string to array of numbers
     * @param type $stringWithNumbers
     */
    private function parseString($stringWithNumbers)
    {
        $this->numbers = $this->parser->parse($stringWithNumbers);
    }

    /**
     * Applies all modifiers
     */
    private function applyModifiers()
    {
        $this->numbers = array_map(function($number) {
            foreach ($this->modifiers as $modifier) {
                $modifier->modify($number);
            }
            return $number;
        }, $this->numbers);
    }

    /**
     * Runs all registred validators
     * @throws InvalidArgumentException
     */
    private function runValidators()
    {
        foreach ($this->validators as $validator) {
            $validator->validate($this->numbers);
        }
    }

    /**
     * Calculates sum of numbers
     * @return type
     */
    private function calculateSum()
    {
        return array_sum($this->numbers);
    }

    /**
     * Creats string calculator object
     *
     * Simply creates StringCalculator object with default modifiers and validators
     * 
     * @return StringCalculator
     */
    public static function instantiate()
    {
        $stringCalculator = new StringCalculator(new ReplaceExplode());

        $allowOnlyPositive = new AllowOnlyPositive();
        $stringCalculator->addModifier($allowOnlyPositive);
        $stringCalculator->addModifier(new SkipLargerThan());
        $stringCalculator->addValidator($allowOnlyPositive);

        return $stringCalculator;
    }

    /**
     * Adds number modifier
     * @param ModifierInterface $modifier
     */
    public function addModifier(ModifierInterface $modifier)
    {
        $this->modifiers[] = $modifier;
    }

    /**
     * Add numbers validator
     * @param ValidatorInterface $validator
     */
    public function addValidator(ValidatorInterface $validator)
    {
        $this->validators[] = $validator;
    }
}