<?php

namespace Jaworek\StringCalculator\Parser;

interface ParserInterface
{
    const DEFAULT_SEPARATORS = [',', "\n"];

    /**
     * Transforms string into array of numbers
     *
     * String must be in correct format. Look at readme.md for more information.
     *
     * @param string $numbersString
     * @return int[] array of numbers
     */
    public function parse($numbersString);
}