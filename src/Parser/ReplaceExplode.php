<?php

namespace Jaworek\StringCalculator\Parser;

use Jaworek\StringCalculator\Exception\InvalidArgumentException;

class ReplaceExplode implements ParserInterface
{
    /**
     * Main separator
     * @var string
     */
    private $mainSeparator;

    /**
     * Array of supporting separators
     * @var array
     */
    private $supportingSeparators;

    /**
     * String with numbers
     * @var string
     */
    private $stringWithNumbers;

    /**
     * Array contains result of parsing
     * @var int[]
     */
    private $numbers;

    public function __construct()
    {
        $this->setSeparators(ParserInterface::DEFAULT_SEPARATORS);
    }

    public function parse($stringWithNumbers)
    {
        $this->stringWithNumbers = $stringWithNumbers;

        $this->detectSeparators();

        $this->replaceSupportingSeparators();

        $this->explodeUsingMainSeparator();

        $this->convertToInt();

        return $this->numbers;
    }

    /**
     * Detects if there are custom separators in string
     */
    private function detectSeparators()
    {
        if (strpos($this->stringWithNumbers, '//') === 0) {
            $stringParts = explode("\n", $this->stringWithNumbers);

            if($stringParts[0] == '//'){
                return false;
            }

            preg_match_all(
                "/\[(.*?)\]/", substr(array_shift($stringParts), 2), $matches
            );

            $this->setSeparators($matches[1]);
            $this->stringWithNumbers = $stringParts[0];
        }
    }

    /**
     * Replaces supporting separator to main separator
     */
    private function replaceSupportingSeparators()
    {
        $this->stringWithNumbers = str_replace($this->supportingSeparators,
            $this->mainSeparator, $this->stringWithNumbers);
    }

    /**
     * Splits string by main seperator
     */
    public function explodeUsingMainSeparator()
    {
        $this->numbers = explode($this->mainSeparator, $this->stringWithNumbers);
    }

    /**
     * Converts all values to int
     */
    public function convertToInt()
    {
        $this->numbers = array_map('intval', $this->numbers);
    }

    /**
     * Sets separators and selects main separator
     * @param array $separators
     */
    private function setSeparators(array $separators)
    {
        $this->mainSeparator        = array_shift($separators);
        $this->supportingSeparators = $separators;
    }
}