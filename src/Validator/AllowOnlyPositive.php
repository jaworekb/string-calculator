<?php

namespace Jaworek\StringCalculator\Validator;

use Jaworek\StringCalculator\Exception\InvalidArgumentException;
use Jaworek\StringCalculator\Modifier\ModifierInterface;


class AllowOnlyPositive implements ModifierInterface, ValidatorInterface
{
    private $negativeNumbers = [];

    /**
     * Finds negative numbers
     * @param type $number
     */
    public function modify(&$number)
    {
        if ($number < 0) {
            $this->negativeNumbers[] = $number;
        }
    }

    /**
     * Throws exception if negative number exists in array
     * 
     * @param array $numbers
     * @throws InvalidArgumentException
     */
    public function validate(array $numbers)
    {
        if (!empty($this->negativeNumbers)) {
            throw new InvalidArgumentException("Numbers can't be lower than 0. Not allowed numbers: ".implode(',',
                $this->negativeNumbers).".");
        }
    }
}