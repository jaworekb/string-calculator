<?php

namespace Jaworek\StringCalculator\Validator;

use Jaworek\StringCalculator\Exception\InvalidArgumentException;

interface ValidatorInterface
{

    /**
     * Validates numbers and throws exception
     * 
     * @param array $numbers
     * @throws InvalidArgumentException
     */
    public function validate(array $numbers);
}