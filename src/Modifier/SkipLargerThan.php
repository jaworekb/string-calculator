<?php

namespace Jaworek\StringCalculator\Modifier;

class SkipLargerThan implements ModifierInterface
{
    const MAXIMUM_ALLOWED = 1000;

    /**
     * Transforms number to 0 if it is larger than MAXIMUM_ALLOWED
     * @param int $number
     */
    public function modify(&$number)
    {
        if ($number > self::MAXIMUM_ALLOWED) {
            $number = 0;
        }
    }
}