<?php

namespace Jaworek\StringCalculator\Modifier;

interface ModifierInterface
{
    /**
     * Modifies given number
     * @param int $number
     */
    public function modify(&$number);
}